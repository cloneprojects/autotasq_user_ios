//
//  Constants.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 21/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Constants {
    //App Constants
//    static var baseURL = "http://159.89.32.146/uber_test/public"
//    static var adminBaseURL = "http://159.89.32.146/uber_test/public/admin"

    static var baseURL = "http://178.62.1.58/uber_test/public"
    static var adminBaseURL = "http://178.62.1.58/uber_test/public/admin"

//    static var mapsKey = "AIzaSyB2Rf4sjfvpGHi_08hOedejghcwd-TFnJA"
    static var socketURL = "http://159.89.32.146:3000/"
//    static var placesKey = "AIzaSyB2Rf4sjfvpGHi_08hOedejghcwd-TFnJA"
//    static var directionsKey = "AIzaSyB2Rf4sjfvpGHi_08hOedejghcwd-TFnJA"
    
    
    static var mapsKey = "AIzaSyAN3zOM6rFTuuzcF5k1_9RzAHXVfE16Iu8"
    static var placesKey = "AIzaSyAN3zOM6rFTuuzcF5k1_9RzAHXVfE16Iu8"
    static var directionsKey = "AIzaSyAN3zOM6rFTuuzcF5k1_9RzAHXVfE16Iu8"

    
    static var locations : [JSON] = []
    static var timeSlots : [JSON] = []
}

func validateEmail(enteredEmail:String) -> Bool {
    
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: enteredEmail)
    
}
