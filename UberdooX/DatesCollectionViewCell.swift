//
//  DatesCollectionViewCell.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 20/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit

class DatesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateBg: UIView!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
}
