//
//  SelectCardViewController.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 12/03/18.
//  Copyright © 2018 Uberdoo. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import SwiftSpinner
import SwiftyJSON

class SelectCardViewController: UIViewController,STPPaymentContextDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var addCardButton: UIButton!
    @IBOutlet weak var addCardView: CardView!
    @IBOutlet weak var cardNotAvailableLabel: UILabel!
    @IBOutlet weak var cardNotAvailableImageView: UIImageView!
    @IBOutlet weak var cardsTableView: UITableView!
    private let customerContext: STPCustomerContext
    private let paymentContext: STPPaymentContext
    var paymentMethods : [STPPaymentMethod] = []
    
    private var price = 0 {
        didSet {
            // Forward value to payment context
            paymentContext.paymentAmount = price
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        let config = STPPaymentConfiguration.shared()
        config.publishableKey = "pk_live_9pufgkCpu4Kjv7vJnTsoxqlm"
        config.companyName = "self.companyName"
        config.requiredBillingAddressFields = STPBillingAddressFields.none
        config.requiredShippingAddressFields = .none
        config.shippingType = STPShippingType.delivery
        
        
        customerContext = STPCustomerContext(keyProvider: MyAPIClient.sharedClient)
        paymentContext = STPPaymentContext(customerContext: customerContext,configuration:config,theme:STPTheme.default())
        
        super.init(coder: aDecoder)
        
        
        
        // Create card sources instead of card tokens
        
        paymentContext.delegate = self
        paymentContext.hostViewController = self
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        cardsTableView.delegate = self
        cardsTableView.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(paymentMethods.count > 0)
        {
            return paymentMethods.count + 1
        }
        else{
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == self.paymentMethods.count)
        {
            self.paymentContext.requestPayment()
        }
        else{
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardsTableViewCell",  for: indexPath) as! CardsTableViewCell

        if(indexPath.row == paymentMethods.count)
        {
            cell.cardNumber.isHidden = true
            cell.cardImage.isHidden = true
            cell.plusIcon.isHidden = false
        }
        else
        {
            cell.cardNumber.isHidden = false
            cell.cardImage.isHidden = false
            cell.plusIcon.isHidden = true
            let stpCard = paymentMethods[indexPath.row] as! STPCard
            
            cell.cardNumber.text = "XXXX - XXXX - XXXX - \(stpCard.last4)"
        }
       
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SwiftSpinner.show("Getting Data")
    }
    
    
    
    
//    func getCards()
//    {
//        var headers : HTTPHeaders!
//        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
//        {
//            headers = [
//                "Authorization": accesstoken,
//                "Accept": "application/json"
//            ]
//        }
//        else
//        {
//            headers = [
//                "Authorization": "",
//                "Accept": "application/json"
//            ]
//        }
//
//
//
//        SwiftSpinner.show("Fetching Cards...")
//        let url = "\(Constants.baseURL)/api/all_cards"
//        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
//
//            if(response.result.isSuccess)
//            {
//                SwiftSpinner.hide()
//                if let json = response.result.value {
//                    print("CARDS JSON: \(json)") // serialized json response
//                    let jsonResponse = JSON(json)
//
//
//                    if(jsonResponse["error"].stringValue == "true")
//                    {
//                        let errorMessage = jsonResponse["error_message"].stringValue
//                        self.showAlert(title: "Failed",msg: errorMessage)
//                    }
//                    else{
//                        self.cards = jsonResponse["data"].arrayValue
//                        if(self.cards.count > 0)
//                        {
//                            self.cardsTableView.reloadData()
//                            self.addCardButton.isHidden = true
//                            self.addCardView.isHidden = true
//                            self.cardNotAvailableLabel.isHidden = true
//                            self.cardNotAvailableImageView.isHidden = true
//                        }
//                        else{
//                            self.addCardButton.isHidden = false
//                            self.addCardView.isHidden = false
//                            self.cardNotAvailableLabel.isHidden = false
//                            self.cardNotAvailableImageView.isHidden = false
//                        }
//
//                    }
//                }
//            }
//            else{
//                SwiftSpinner.hide()
//                print(response.error.debugDescription)
//                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
//            }
//        }
//    }
    
    @IBAction func addCard(_ sender: Any) {
        print("Hello")
        self.paymentContext.requestPayment()
            
    }
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
            SwiftSpinner.hide()
        
            if let paymentmethods = paymentContext.paymentMethods
            {
                
                paymentMethods = paymentmethods
//                print(paymentMethods)
                if(paymentMethods.count > 0)
                {
                    self.cardsTableView.reloadData()
                    self.addCardButton.isHidden = true
                    self.addCardView.isHidden = true
                    self.cardNotAvailableLabel.isHidden = true
                    self.cardNotAvailableImageView.isHidden = true
                }
                else{
                    self.addCardButton.isHidden = false
                    self.addCardView.isHidden = false
                    self.cardNotAvailableLabel.isHidden = false
                    self.cardNotAvailableImageView.isHidden = false
                }

            }
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext,
                        didCreatePaymentResult paymentResult: STPPaymentResult,
                        completion: @escaping STPErrorBlock) {
        print(paymentResult)
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func paymentContext(_ paymentContext: STPPaymentContext,
                        didFinishWith    status: STPPaymentStatus,
                        error: Error?) {
        
        switch status {
        case .error:
            //            self.showError(error)
            print(error)
        case .success:
            print("Success")
        //            self.showReceipt()
        case .userCancellation:
            return // Do nothing
        }
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext,
                        didFailToLoadWithError error: Error) {
        self.navigationController?.popViewController(animated: true)
        // Show the error to your user, etc.
    }

}
