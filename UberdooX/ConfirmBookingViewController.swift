//
//  ConfirmBookingViewController.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 17/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import SwiftSpinner
import Alamofire
import Nuke

class ConfirmBookingViewController: UIViewController {

    @IBOutlet weak var addressImageView: UIImageView!
    @IBOutlet weak var bookingSlot: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var providerImage: UIImageView!
    @IBOutlet weak var pricing: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var addressTitleLbl: UILabel!
    var subCategoryName : String!
    var subCategoryId : String!
    var date : String!
    var timeSlot : String!
    var timeSlotId : String!
    var pricePerHour : String!
    var address : [String:JSON]!
    var addressId : String!
    var providerId : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(subCategoryId)
        print(timeSlotId)
        print(subCategoryId)
        print(date)
   
        providerId = ProviderProfileViewController.providerDetails["id"]?.stringValue
        print(providerId!)
        print(addressId)
        
        providerName.text = ProviderProfileViewController.providerDetails["name"]?.stringValue
        serviceName.text = self.subCategoryName
        bookingDate.text = self.date
        bookingSlot.text = self.timeSlot
        addressLbl.text = self.address["address_line_1"]?.stringValue
        addressTitleLbl.text = "(\(self.address["title"]!.stringValue))"
        pricing.text = "GHS \(ProviderProfileViewController.providerDetails["priceperhour"]?.stringValue ?? "0")"
        // Do any additional setup after loading the view.
        
        if let imageName = ProviderProfileViewController.providerDetails["image"]?.string
        {
            if let imageURL = URL.init(string: imageName)
            {
                Nuke.loadImage(with: imageURL, into: providerImage)
            }
        }
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let lat = self.address["latitude"]!.stringValue
        let long = self.address["longitude"]!.stringValue
        
        
        let styleMapUrl: String = "https://maps.googleapis.com/maps/api/staticmap?sensor=false&size=\(2 * Int(self.addressImageView.frame.size.width))x\(2 * Int(self.addressImageView.frame.size.height))&zoom=15&center=\(lat),\(long)&style=feature:administrative%7Celement:geometry%7Ccolor:0x1d1d1d%7Cweight:1&style=feature:administrative%7Celement:labels.text.fill%7Ccolor:0x93a6b5&style=feature:landscape%7Ccolor:0xeff0f5&style=feature:landscape%7Celement:geometry%7Ccolor:0xdde3e3%7Cvisibility:simplified%7Cweight:0.5&style=feature:landscape%7Celement:labels%7Ccolor:0x1d1d1d%7Cvisibility:simplified%7Cweight:0.5&style=feature:landscape.natural.landcover%7Celement:geometry%7Ccolor:0xfceff9&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels%7Cvisibility:off%7Cweight:0.5&style=feature:poi%7Celement:labels.text%7Ccolor:0x505050%7Cvisibility:off&style=feature:poi.attraction%7Celement:labels%7Cvisibility:off&style=feature:poi.attraction%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:off&style=feature:poi.business%7Celement:labels%7Cvisibility:off&style=feature:poi.business%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:off&style=feature:poi.government%7Celement:labels%7Cvisibility:off&style=feature:poi.government%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:off&style=feature:poi.medical%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.park%7Celement:geometry%7Ccolor:0xa9de82&style=feature:poi.park%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.place_of_worship%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.school%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.sports_complex%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road%7Celement:labels.text%7Ccolor:0xc0c0c0%7Cvisibility:simplified%7Cweight:0.5&style=feature:road%7Celement:labels.text.fill%7Ccolor:0x000000&style=feature:road.highway%7Celement:geometry%7Ccolor:0xf4f4f4%7Cvisibility:simplified&style=feature:road.highway%7Celement:labels.text%7Ccolor:0x1d1d1d%7Cvisibility:simplified&style=feature:road.highway.controlled_access%7Celement:geometry%7Ccolor:0xf4f4f4&style=feature:transit%7Celement:geometry%7Ccolor:0xc0c0c0&style=feature:water%7Celement:geometry%7Ccolor:0xa5c9e1"
        
        
            print(styleMapUrl)
            let url = URL(string: styleMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
            Nuke.loadImage(with: url!, into: self.addressImageView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func confirmBooking(_ sender: Any) {
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let params: Parameters = [
            "service_sub_category_id": self.subCategoryId,
            "time_slot_id": self.timeSlotId,
            "date":self.date,
            "provider_id": self.providerId,
            "address_id": self.addressId,
        ]
        SwiftSpinner.show("Sending your request...")
        let url = "\(Constants.baseURL)/api/newbooking"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("CONFIRM BOOKING JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingRequestViewController") as! BookingRequestViewController
                        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        self.present(vc, animated: true, completion: nil)
                    
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
