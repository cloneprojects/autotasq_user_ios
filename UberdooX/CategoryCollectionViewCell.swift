//
//  CategoryCollectionViewCell.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 20/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
}
