//
//  HomeViewController.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 13/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON
import Nuke
import UserNotifications


class HomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UNUserNotificationCenterDelegate,UIScrollViewDelegate {
//    var itemInfo = IndicatorInfo(title: "View")

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bannersView: ScalingCarouselView!
    @IBOutlet weak var tableView: UITableView!
    var categories : [JSON] = []
    var banners : [JSON] = []
    var locations : [JSON] = []
    var categoryGroups : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
    
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate  = self
        }
        
       
        bannersView.delegate = self
        bannersView.dataSource = self
        if(MainViewController.status.count > 0)
        {
            let statusDict = MainViewController.status[0].dictionary

        
            let currentStatus = statusDict!["status"]?.stringValue
            if(currentStatus == "Completedjob")
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceViewController") as! InvoiceViewController
                vc.bookingDetails = statusDict
                vc.modalPresentationStyle = .overCurrentContext
                self.present(vc, animated: true, completion: nil)
            }
            else if(currentStatus == "Waitingforpaymentconfirmation"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WaitingForPaymentConfirmationViewController") as! WaitingForPaymentConfirmationViewController
                vc.bookingDetails = statusDict
                self.present(vc, animated: true, completion: nil)
            }
            else if(currentStatus == "Reviewpending"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as! ReviewViewController
                vc.bookingDetails = statusDict
                self.present(vc, animated: true, completion: nil)
            }
            else{
                getHomeDetails()
            }
        }
        else{
            getHomeDetails()
        }
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override var preferredContentSize: CGSize{
        get {
            self.tableView.layoutIfNeeded()
            return self.tableView.contentSize
        }
        set {}
    }
    
    func getHomeDetails(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
             headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
             headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }

        
        SwiftSpinner.show("Fetching Services...")
        let url = "\(Constants.baseURL)/api/homedashboard"
        Alamofire.request(url,method: .get,headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("HOME JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    
                    
                    self.locations = jsonResponse["location"].arrayValue
                    let catarray = jsonResponse["list_category"].arrayValue
                    self.banners = jsonResponse["banner_images"].arrayValue
                    self.bannersView.reloadData()
                    let rootdict = catarray[0].dictionary as NSDictionary!
                    
                    self.categoryGroups = rootdict!.allKeys as! [String]
                    let image = jsonResponse["image"].string
                    UserDefaults.standard.set(image, forKey: "image")
                    let tableHeight = self.categoryGroups.count * 170;
                    self.tableView.contentSize = CGSize.init(width: 320, height: tableHeight)
                    self.tableView.frame = CGRect.init(x: self.tableView.frame.origin.x, y: self.tableView.frame.origin.y, width: self.tableView.frame.size.width, height: CGFloat(tableHeight))
                    self.tableView.layoutIfNeeded()
                    let scrollViewHeight = Float(self.bannersView.frame.size.height) + Float(tableHeight) + 50.0
                    self.scrollView.contentSize = CGSize.init(width: 320, height: Int(scrollViewHeight))
                    for cat in self.categoryGroups{
                        let group = rootdict![cat]
                        
                        self.categories.append(group! as! JSON)
                    }
                    self.tableView.reloadData()                    
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryGroups.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            guard let categoryCell = cell as? HomeTableViewCell else { return }
            categoryCell.setCollectionView(dataSourceDelegate: self, forRow: indexPath.row)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.groupTitle.text = self.categoryGroups[indexPath.row]
        cell.categoryCollectionView.tag = indexPath.row
        return cell
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        bannersView.didScroll()
        guard let currentCenterIndex = bannersView.currentCenterCellIndex?.row else { return }
        
        
    }
    


}
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let index = collectionView.tag
        
        if(index == -1)
        {
            return self.banners.count
        }
        else{
            return self.categories[index].count
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let index = collectionView.tag
        
            if(index == -1)
            {
             
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
                cell.bannerName.text = self.banners[indexPath.row]["banner_name"].stringValue
                if let imageName = self.banners[indexPath.row]["banner_logo"].string
                {
                    if let imageUrl = URL.init(string: imageName)
                    {
                            Nuke.loadImage(with: imageUrl, into: cell.bannerImage)
                    }
                }
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
                let category = self.categories[index]
                cell.categoryName.text = category[indexPath.row]["category_name"].stringValue
                if let imageString = category[indexPath.row]["icon"].string
                {
                    if let imageURL = URL(string:imageString)
                    {
                        Nuke.loadImage(with: imageURL, into: cell.categoryImage)
                    }
                }
                return cell
            }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = collectionView.tag
        if(index != -1)
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoriesViewController") as! SubCategoriesViewController
            
            let category = self.categories[index]
            vc.subCategoryId = category[indexPath.row]["id"].stringValue
            self.present(vc, animated: true, completion: nil)
        }
    }

    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        getAppSettings()
        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue) | UInt8(UNNotificationPresentationOptions.sound.rawValue))))
        
    }
    
    func getAppSettings(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let url = "\(Constants.baseURL)/api/appsettings"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                if let json = response.result.value {
                    print("APP SETTINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        Constants.locations = jsonResponse["location"].arrayValue
                        Constants.timeSlots = jsonResponse["timeslots"].arrayValue
                        
                        let statusArray = jsonResponse["status"].arrayValue;
                        if(statusArray.count > 0){
                            let statusDict = statusArray[0].dictionary
                            let currentStatus = statusDict!["status"]?.stringValue
                            if(currentStatus == "Completedjob")
                            {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceViewController") as! InvoiceViewController
                                vc.bookingDetails = statusDict
                                vc.modalPresentationStyle = .overCurrentContext
                                self.present(vc, animated: true, completion: nil)
                            }
                            else if(currentStatus == "Waitingforpaymentconfirmation"){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WaitingForPaymentConfirmationViewController") as! WaitingForPaymentConfirmationViewController
                                vc.bookingDetails = statusDict
                                self.present(vc, animated: true, completion: nil)
                            }
                            else if(currentStatus == "Reviewpending"){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as!     ReviewViewController
                                vc.bookingDetails = statusDict
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
            else{
                print(response.error!.localizedDescription)
                //                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
}

