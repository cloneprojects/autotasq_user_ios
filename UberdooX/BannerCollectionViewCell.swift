//
//  BannerCollectionViewCell.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 20/12/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerName: UILabel!
    @IBOutlet weak var bannerImage: UIImageView!
}
