//
//  EditAddressViewController.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 17/11/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import SwiftSpinner
import Alamofire

class EditAddressViewController: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate,GMSMapViewDelegate {

    var addressTitle : String!
    var doorNo : String!
    var addressLandmark : String!
    var addressLine : String!
    
    @IBOutlet weak var titleFld: UITextField!
    @IBOutlet weak var crossHair: UIButton!
    @IBOutlet weak var changeAddress: UIButton!
    @IBOutlet weak var addressCardView: CardView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var landmarkFld: UITextField!
    @IBOutlet weak var doorNoFld: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    var currentAddress : String!
    @IBOutlet weak var currentAddressLbl: UILabel!
    var locationManager : CLLocationManager!
    var currentLatitude : String!
    var currentLongitude : String!
    var selectedLatitude = ""
    var selectedLongitude = ""
    var foundCurrentLocation = false
    var isShowing = false
    @IBOutlet weak var locationIndicator: UIImageView!
    var marker:GMSMarker!
    
    var addressId : String!
    
    var existingApiCall = false;
    var centerMapCoordinate:CLLocationCoordinate2D!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        print(addressId)
        doorNoFld.delegate = self
        landmarkFld.delegate = self
        
        mapView.isMyLocationEnabled = true
        
        doorNoFld.text = doorNo
        titleFld.text = addressTitle
        landmarkFld.text = addressLandmark
        currentAddressLbl.text = addressLine
        print("addressLine\(addressLine)")
        do{
            let filePath: String? = Bundle.main.path(forResource: "map", ofType: "json")
            
            let url = URL.init(fileURLWithPath: filePath!)
            mapView.mapStyle = try GMSMapStyle.init(contentsOfFileURL: url)
            mapView.delegate = self
        }
        catch{
            print(error)
        }
        

        let lat = Double(currentLatitude)
        let lng = Double(currentLongitude)
        let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lng!, zoom: 15.0)
        //            print(camera)
        if(mapView != nil && !foundCurrentLocation)
        {
            foundCurrentLocation = true
            mapView.camera = camera
        }
        
        locationIndicator.layer.shadowColor = UIColor.gray.cgColor
        locationIndicator.layer.shadowOffset = CGSize(width: 0, height: 1)
        locationIndicator.layer.shadowOpacity = 1
        locationIndicator.layer.shadowRadius = 1.0
        locationIndicator.clipsToBounds = false
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        if(locationManager != nil){
//            locationManager.stopUpdatingLocation()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        if(!existingApiCall)
        {
            existingApiCall = true
            getAddressFor(location: centerMapCoordinate)
        }
        //        self.placeMarkerOnCenter(centerMapCoordinate:centerMapCoordinate)
    }
    
    func getAddressFor(location : CLLocationCoordinate2D){
        GMSGeocoder().reverseGeocodeCoordinate(location) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                
                self.selectedLatitude = String(location.latitude)
                self.selectedLongitude = String(location.longitude)
                
                self.currentAddress = lines.joined(separator: " ")
                print("currentAddress\(self.currentAddress)")
                self.currentAddressLbl.text = self.currentAddress
                print(self.currentAddress)
                self.existingApiCall = false
            }
        }
    }
    func placeMarkerOnCenter(centerMapCoordinate:CLLocationCoordinate2D) {
        if marker == nil {
            marker = GMSMarker()
        }
        marker.position = centerMapCoordinate
        marker.map = self.mapView
    }
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField
        {
        case doorNoFld:
            landmarkFld.becomeFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
  
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addAddress(_ sender: Any) {
        if(titleFld.text == "")
        {
            self.showAlert(title: "Validation Failed", msg: "Title is required")
        }
        else if(doorNoFld.text == "")
        {
            self.showAlert(title: "Validation Failed", msg: "Door No. is required")
        }
        else{
            print("Confirm")
            saveAddress()
        }
    }
    
    func saveAddress(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        print(UserDefaults.standard.string(forKey: "access_token") as String!)
        var lmark : String!
        let city = "Chennai"
        if let landmark = self.landmarkFld.text
        {
            lmark = landmark
        }
        else{
            lmark = ""
        }
        print(self.currentAddress)
        print(city)
        print(self.doorNoFld.text!)
        print(lmark)
        print(self.titleFld.text!)
        print(currentLatitude)
        print(currentLongitude)
        if(selectedLatitude == "")
        {
            selectedLatitude = currentLatitude
        }
        if(selectedLongitude == "")
        {
            selectedLongitude = currentLongitude
        }
        let params: Parameters = [
            "address": self.currentAddress,
            "id":addressId,
            "doorno": self.doorNoFld.text!,
            "landmark":lmark,
            "title":self.titleFld.text!,
            "latitude": selectedLatitude,
            "longitude": selectedLongitude
        ]
        SwiftSpinner.show("Saving your address...")
        print(params)
        let url = "\(Constants.baseURL)/api/updateaddress"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("UPDATE ADDRESS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    @IBAction func showHideBottomView(_ sender: Any) {
        
        if(isShowing)
        {
            isShowing = false
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.showHideTransitionViews, animations: {
                
                self.mapView.isUserInteractionEnabled = true
                
                self.bottomView.transform = CGAffineTransform(translationX: 0, y: 0)
                self.crossHair.transform = CGAffineTransform(translationX: 0, y: 0)
                self.locationIndicator.transform = CGAffineTransform(translationX: 0, y: 0)
                self.mapView.transform = CGAffineTransform(translationX: 0, y: 0)
                self.changeAddress.transform = CGAffineTransform(translationX: 0, y: 0)
            }) { (Void) in
                
            }
        }
        else{
            isShowing = true
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.showHideTransitionViews, animations: {
                self.mapView.isUserInteractionEnabled = false
                
                self.bottomView.transform = CGAffineTransform(translationX: 0, y: -270)
                self.crossHair.transform = CGAffineTransform(translationX: 0, y: -270)
                self.locationIndicator.transform = CGAffineTransform(translationX: 0, y: -100)
                self.mapView.transform = CGAffineTransform(translationX: 0, y: -100)
                self.changeAddress.transform = CGAffineTransform(translationX: 0, y: -270)
                
            }) { (Void) in
                
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        currentLatitude = String(coord.latitude)
        currentLongitude = String(coord.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: coord.latitude, longitude: coord.longitude, zoom: 15.0)
        //            print(camera)
        if(mapView != nil && !foundCurrentLocation)
        {
            foundCurrentLocation = true
            mapView.camera = camera
        }
    }
    
    @IBAction func moveCameraToCurrentLocation(_ sender: Any) {
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(currentLatitude)!, longitude: Double(currentLongitude)!, zoom: 15.0)
        //            print(camera)
        if(mapView != nil)
        {
            mapView.animate(to: camera)
            getAddressFor(location: centerMapCoordinate)
        }
    }
    @IBAction func searchLocation(_ sender: Any) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print(error)
    }


}
