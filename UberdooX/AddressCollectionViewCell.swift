//
//  AddressCollectionViewCell.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 20/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit

class AddressCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var addressTitleLbl: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var plusImgView: UIImageView!
    @IBOutlet weak var addressbl: UILabel!
    @IBOutlet weak var addNewAddressLbl: UILabel!
    @IBOutlet weak var cardSelectedImgView: UIImageView!
}
