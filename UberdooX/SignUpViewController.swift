//
//  SignUpViewController.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 12/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner


class SignUpViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var passwordFld: UITextField!
    @IBOutlet weak var phoneFld: UITextField!
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var firstNameFld: UITextField!
    @IBOutlet weak var lastNameFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailFld.delegate = self
        passwordFld.delegate = self
        phoneFld.delegate = self
        firstNameFld.delegate = self
        lastNameFld.delegate = self


        // Do any additional setup after loading the view.
    }

//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
//    }
//        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField
        {
        case firstNameFld:
            lastNameFld.becomeFirstResponder()
            break
        case lastNameFld:
            emailFld.becomeFirstResponder()
            break
        case emailFld:
            phoneFld.becomeFirstResponder()
            break
        case phoneFld:
            passwordFld.becomeFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }

    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func validateForm() -> Bool{
        
        let Email = emailFld.text!
        let Emailvalid = validateEmail(enteredEmail: Email)
        
        if (firstNameFld.text?.isEmpty)!
        {
            showAlert(title: "Validation Failed",msg: "Enter Firstname")
            return false
        }
        else if (lastNameFld.text?.isEmpty)!
        {
            showAlert(title: "Validation Failed",msg: "Enter Lastname")
            return false
        }
        else if (emailFld.text?.isEmpty)!
        {
            showAlert(title: "Validation Failed",msg: "Enter Email Id")
            return false
        }
        else if Emailvalid == false{
            showAlert(title: "Validation Failed",msg: "Invalid Email Id")
            return false
        }
        else if (phoneFld.text?.isEmpty)!
        {
            showAlert(title: "Validation Failed",msg: "Enter Phone")
            return false
        }
        else if (passwordFld.text?.isEmpty)!
        {
            showAlert(title: "Validation Failed",msg: "Enter Password")
            return false
        }
        else if passwordFld.text!.count < 6 {
            showAlert(title: "Validation Failed",msg: " Password should be minimum 6 characters ")
            return false
        }
        else{
            return true
        }
    }
    @IBAction func signUp(_ sender: Any) {
        let isValid = validateForm()
        if(isValid)
        {
            
//            emailFld.text! = emailFld.text!.stringByReplacingOccurrencesOfString(" ", withString: "")

            
            let params: Parameters = [
                "first_name": firstNameFld.text!,
                "last_name": lastNameFld.text!,
                "email": emailFld.text!,
                "mobile": phoneFld.text!,
                "password": passwordFld.text!
            ]
            
            let url = "\(Constants.baseURL)/api/signup"
            SwiftSpinner.show("Signing up...")

            Alamofire.request(url,method: .post,parameters:params).responseJSON { response in
                
//                print("Result: \(response.result)")                         // response serialization result
                if(response.result.isSuccess)
                {
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("SIGNUP JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true")
                        {
                            let errorMessage = jsonResponse["error_message"].stringValue
                            self.showAlert(title: "Signup Failed",msg: errorMessage)
                        }
                        else{
                            let alert = UIAlertController(title: "Signup Successful", message: "Please login", preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                                (alert: UIAlertAction!) in
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                                self.present(vc, animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
                
            }
        }
    }

    @IBAction func goToLoginPage(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        self.present(vc, animated: true, completion: nil)
    }
}
