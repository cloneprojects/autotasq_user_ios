//
//  AccountViewController.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 13/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON
import Nuke
import UserNotifications

class AccountViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UNUserNotificationCenterDelegate,updateImageDelegate {
    
    
    func updateImage() {
        
        if let image = UserDefaults.standard.string(forKey: "image") as String!{
            
            if let imageUrl = URL.init(string: image) as URL!{
                Nuke.loadImage(with: imageUrl, into: self.userPicture)
            }
        }
        if let first_name = UserDefaults.standard.string(forKey: "first_name") as String!
        {
            if let  last_name = UserDefaults.standard.string(forKey: "last_name") as String!{
                nameLbl.text = "\(String(describing: first_name)) \(String(describing: last_name))"
            }
            else{
                nameLbl.text = "\(String(describing: first_name))"
            }
        }
        else{
            nameLbl.text = "Guest User"
        }
        
    }
    
    
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userPicture: UIImageView!
    var titles :[String] = []
    var images :NSArray!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if let image = UserDefaults.standard.string(forKey: "image") as String!{
            
            if let imageUrl = URL.init(string: image) as URL!{
                Nuke.loadImage(with: imageUrl, into: self.userPicture)
            }
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        let isLoggedInSkipped = UserDefaults.standard.bool(forKey: "isLoggedInSkipped")
        if(isLoggedInSkipped)
        {
            self.titles = NSArray(objects: "About us","Help and FAQ","Login") as! [String]
            self.images = NSArray(objects:"new_about_us","info","new_logout")
        }
        else{
            self.titles = NSArray(objects: "Profile","Change Password","Addresses","About Us","Help and FAQ","Logout") as! [String]
            self.images = NSArray(objects:"new_profile","new_change_passsword","new_address","new_about_us","info","new_logout")
        }
        
        

//        let height = self.titles.count * 70;
//        tableView.frame = CGRect.init(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: CGFloat(height))
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let image = UserDefaults.standard.string(forKey: "image") as String!{
            
            if let imageUrl = URL.init(string: image) as URL!{
                Nuke.loadImage(with: imageUrl, into: self.userPicture)
            }
        }
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
        if let first_name = UserDefaults.standard.string(forKey: "first_name") as String!
        {
            if let  last_name = UserDefaults.standard.string(forKey: "last_name") as String!{
                nameLbl.text = "\(String(describing: first_name)) \(String(describing: last_name))"
            }
            else{
                nameLbl.text = "\(String(describing: first_name))"
            }
        }
        else{
            nameLbl.text = "Guest User"
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTableViewCell", for: indexPath) as! AccountTableViewCell
        cell.title.text = self.titles[indexPath.row]
        let imagename = self.images.object(at: indexPath.row) as! String
        cell.icon.image = UIImage(named:imagename)

        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titles.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       //self.titles = NSArray(objects: "View Profile","Change Password","About Us","Help and FAQ","Logout")
        
        if(self.titles[indexPath.row] == "Profile")
        {
            print(self.titles[indexPath.row])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            MainViewController.changePage = false
             vc.updateDelegate = self
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "Change Password")
        {
            print(self.titles[indexPath.row])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
        }
        else if(self.titles[indexPath.row] == "About Us")
        {
//            print(self.titles[indexPath.row])
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            vc.titleString = "About Us"
//            MainViewController.changePage = false
//            self.present(vc, animated: true, completion: nil)
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
            vc.titleVal = "About Us"
            vc.message = "AutoTasQ is a mobile marketplace that matches freelance labor with local demand, allowing consumers to find immediate help with everyday tasks, including cleaning, moving, delivery and handyman work."
            
            self.present(vc, animated: true, completion: nil)
            print(self.titles[indexPath.row])
            
            
        }
        else if(self.titles[indexPath.row] == "Help and FAQ")
        {
//            print(self.titles[indexPath.row])
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
//            vc.titleString = "Help and FAQ"
//            MainViewController.changePage = false
//            self.present(vc, animated: true, completion: nil)
//            print(self.titles[indexPath.row])
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FAQViewController") as! FAQViewController
            vc.titleVal = "Help and FAQ"
            vc.message = "What should I do to prepare for my cleaning?\n\nTake care to clean up any clutter like dirty clothes or dirty dishes so the provider can focus on cleaning. Make sure anything that needs to be done for the provider to get access into all areas that require cleaning has been taken care of.\n\nWhat’s included in my cleaning?\n\nThis should be discussed with the selected provider and each provider has a profile which you can view their services.\n\nHow do I pay? Should I give my cleaner a tip?\n\nAutotasq only acts as a connection tool to local providers so all payment should be discussed with your selected provider. The providers hourly rates are displayed on their profile and are calculated based upon this rate. Autotasq then takes a 15% referral fee from the providers. We don’t require tipping, but if you feel your provider has gone above and beyond, we won’t stop you!\n\nWhat cities do you currently cover?\n\nOur providers are currently located throughout the UK and are always expanding so you can view providers available in your area by setting a radius.\n\nWhat if I need to cancel my cleaning?\n\nYou can cancel the provider in the app but are restricted from cancelling on the day of the scheduled providers arrival. Cancellation charges may be implemented in the future based on users feedback.\n\nWhich provider will come to my place?\n\nThe user is in charge of selecting the provider and also can offer jobs out to all local providers where in this case the provider who selects the job will arrive. You can also track providers on route in the app.\n\nCan I schedule regular cleanings?\n\nYes! We provide recurring cleanings that you can schedule for monthly service, bi-weekly or weekly service.\n\nAre providers insured?\n\nWe require all of our providers to have insurance but it is the user’s responsibility to check with selected providers prior to any jobs being started/undertaken as Autotasq only acts to connect users with providers and no providers are directly employed by Autotasq.\n\nDo I need to be at my home for my cleaning?\n\nYes unless you have made own arrangements with the selected provider.\n\nHow is the price calculated?\n\nOur cleanings are priced by the hour, and all providers display they hourly rate on their profile\n\nI’m not satisfied with my service. What do I do?\n\nYou should contact the provider direct and after completion of all jobs you get the chance to review providers as providers can also review the users. Autotasq takes feedback very seriously and any provider receiving poor feedback on a regular occurrence will be suspended from the platform and we don’t give any second chances once off your off."
            
            self.present(vc, animated: true, completion: nil)
            print(self.titles[indexPath.row])
            
            
        }
        else if(self.titles[indexPath.row] == "Addresses")
        {
            print(self.titles[indexPath.row])
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressesViewController") as! AddressesViewController
            MainViewController.changePage = false
            self.present(vc, animated: true, completion: nil)
            print(self.titles[indexPath.row])
        }
        else if(self.titles[indexPath.row] == "Logout" || self.titles[indexPath.row] == "Login")
        {
            print(self.titles[indexPath.row])
            let isLoggedInSkipped = UserDefaults.standard.bool(forKey: "isLoggedInSkipped")
            UserDefaults.standard.set(false, forKey: "isLoggedInSkipped")
            if(isLoggedInSkipped)
            {
                self.login()
            }
            else{
                self.logout()
            }
            
        }

    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func login(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OnboardViewController") as! OnboardViewController
        self.present(vc, animated: true, completion: nil)
    }
    func logout()
    {
        let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        print(accesstoken!)
        let headers: HTTPHeaders = [
            "Authorization": accesstoken!,
            "Accept": "application/json"
        ]

        SwiftSpinner.show("Logging out...")
        let url = "\(Constants.baseURL)/api/logout"
        Alamofire.request(url,method: .get,headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("LOGOUT JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    if(jsonResponse["error"].stringValue == "Unauthenticated" || jsonResponse["error"].stringValue == "true")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        getAppSettings()
        //        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue) | UInt8(UNNotificationPresentationOptions.sound.rawValue))))
        
    }
    
    func getAppSettings(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let url = "\(Constants.baseURL)/api/appsettings"
        Alamofire.request(url,method: .get, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                if let json = response.result.value {
                    print("APP SETTINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        Constants.locations = jsonResponse["location"].arrayValue
                        Constants.timeSlots = jsonResponse["timeslots"].arrayValue
                        
                        let statusArray = jsonResponse["status"].arrayValue;
                        if(statusArray.count > 0){
                            let statusDict = statusArray[0].dictionary
                            let currentStatus = statusDict!["status"]?.stringValue
                            if(currentStatus == "Completedjob")
                            {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceViewController") as! InvoiceViewController
                                vc.bookingDetails = statusDict
                                vc.modalPresentationStyle = .overCurrentContext
                                self.present(vc, animated: true, completion: nil)
                            }
                            else if(currentStatus == "Waitingforpaymentconfirmation"){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "WaitingForPaymentConfirmationViewController") as! WaitingForPaymentConfirmationViewController
                                vc.bookingDetails = statusDict
                                self.present(vc, animated: true, completion: nil)
                            }
                            else if(currentStatus == "Reviewpending"){
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewViewController") as!     ReviewViewController
                                vc.bookingDetails = statusDict
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
            else{
                print(response.error!.localizedDescription)
                //                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                
                self.present(vc, animated: true, completion: nil)
            }
        }
    }

}
