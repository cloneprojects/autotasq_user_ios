//
//  SelectProvidersCollectionCell.swift
//  SwyftX
//
//  Created by Karthik Sakthivel on 16/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import Cosmos

class SelectProvidersCollectionCell: ScalingCarouselCell {
    
    @IBOutlet weak var distancelabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var providerImage: UIImageView!
}
